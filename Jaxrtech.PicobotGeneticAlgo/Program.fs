﻿module Jaxrtech.PicobotGeneticAlgo.Program

open FSharp.Data

open ModelTypes
open ModelFunctions


[<EntryPoint>]
let main argv =
    let unsortedMaps =
        JsonValue.Load("maps.json")
        |> GridSet.fromJson

    let order =
        ["emptymap"; "diamondmap"; "mazemap"; "map3"; "map4"; "map5"; "map6"]

    let maps = new ResizeArray<(string * Grid)>()
    
    order
    |> List.iter (fun name ->
        let grid = unsortedMaps |> Map.tryFind name |> Option.get
        (name, grid) |> maps.Add)

    maps
    |> Seq.iter (fun (k, v) ->
        v
        |> Grid.visualize
        |> printfn "%s:\n%s" k)

    //

    let emptyMap = maps |> Seq.head |> snd
    
    let position =
        emptyMap
        |> Grid.findIndices Cell.isEmpty
        |> Random.pick
        |> Position

    let (->>) = Rule.create

    let rules =
        [(State 0, [North, Empty]) ->> (North, State 0)]
        |> Map.ofList
        |> RuleSet

    let robot =
        { Position = position
        ; State = State 0
        ; Rules = rules
        }

    let world =
        { Grid = emptyMap
        ; VisitedCells = Seq.empty
        ; Robot = robot
        }

    world
    |> AttributedGrid.ofWorld
    |> AttributedGrid.visualize
    |> printfn "%s\n"

    System.Console.ReadKey() |> ignore
    0
