﻿module Jaxrtech.PicobotGeneticAlgo.ModelTypes

open System.Collections.ObjectModel


type Direction =
     | North
     | East
     | West
     | South


type State = State of int


type Cell =
     | Wall
     | Empty


type CellAttribute =
     | Robot
     | Visited


type CellAttributeSet = CellAttributeSet of Set<CellAttribute>


type AttributedCell = AttributedCell of Cell * CellAttributeSet


type RuleKey = {
    Initial: State
    Surroundings: Map<Direction, Cell>
}


type RuleValue = {
    Direction: Direction
    Next: State
}


type Rule = Rule of RuleKey * RuleValue


type RuleSet = RuleSet of Map<RuleKey, RuleValue>


type Grid = Grid of Cell[,]


type AttributedGrid = AttributedGrid of AttributedCell[,]


type Position = Position of int * int


type Robot = {
    Position: Position
    State: State
    Rules: RuleSet
}


type World = {
    Grid: Grid
    VisitedCells: seq<Position>
    Robot: Robot
}