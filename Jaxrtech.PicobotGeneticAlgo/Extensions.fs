﻿[<AutoOpen>]
module Jaxrtech.PicobotGeneticAlgo.Extensions


module Set =
    let tryFind x set =
        if set |> Set.contains x
        then Some(x)
        else None


module Option =
    let ifNone x = function
        | Some a -> a
        | None -> x


module Random =
    open System

    let pick xs =
        let length = xs |> Seq.length
        
        let r = new Random()
        let i = r.Next(0, length)

        xs |> Seq.item i