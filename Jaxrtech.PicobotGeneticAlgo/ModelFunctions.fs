﻿module Jaxrtech.PicobotGeneticAlgo.ModelFunctions

open ModelTypes

module State =
    let get (State(i)) = i

    let isValid (State(i)) = i >= 0 && i <= 99


module Cell =
    let isWall = function
        | Wall -> true
        | _ -> false

    let isEmpty = function
        | Empty -> true
        | _ -> false

    let ofInternal i =
        match i with
        | 0 -> Cell.Empty |> Some
        | 1 -> Cell.Wall |> Some
        | _ -> None

    let toChar = function
        | Wall -> 'x'
        | Empty -> ' '   


module CellAttribute =
    let toChar = function
        | Robot -> '@'
        | Visited -> '.'

    let displayOrder =
        [Robot; Visited]


module CellAttributeSet =
    let private orderedTry xs f (CellAttributeSet(attrs)) =
        let rec loop = function
            | [] ->
                None

            | attr::rest ->
                let x = attrs |> Set.tryFind attr |> Option.map f
                match x with
                | Some _ -> x
                | None -> loop rest
        loop xs

    let empty =
        Set.empty |> CellAttributeSet

    let toChar =
        orderedTry CellAttribute.displayOrder CellAttribute.toChar


module AttributedCell =
    let toChar (AttributedCell(cell, attrs)) =
        attrs |> CellAttributeSet.toChar
        |> Option.ifNone (cell |> Cell.toChar)


module GridSet =
    open FSharp.Data

    let fromJson (json: JsonValue) =
        json.Properties()
        |> Map.ofArray

        |> Map.map (fun _ rows ->
            rows.AsArray()
            |> Array.map (fun cols ->
                cols.AsArray()
                |> Array.map (fun x -> x.AsInteger())))

        |> Map.map (fun _ grid ->
            let rows = grid.Length
            let cols = grid.[0].Length

            Array2D.init cols rows (fun c r -> grid.[r].[c]))

        |> Map.map (fun _ grid ->
            grid
            |> Array2D.map (Cell.ofInternal >> Option.get)
            |> Grid)


module Array2D =
    let iterij inner outer array =
        let coli = (array |> Array2D.length1) - 1
        let rowi = (array |> Array2D.length2) - 1

        for y in {0..rowi} do
            for x in {0..coli} do
                inner x y (array.[x, y])

            outer y

    let iterf inner outer =
        iterij 
            (fun _ _ x -> inner x)
            (fun _ -> outer ())


module Grid =
    open System.Text

    let findIndices (predicate: Cell -> bool) (Grid(grid)) : seq<(int * int)> =
        let idx = new ResizeArray<(int * int)>()

        grid
        |> Array2D.map predicate
        |> Array2D.iteri (fun y x _ -> (y, x) |> idx.Add)

        upcast idx

    let visualizef (f: 'a -> char) grid =
        let cells = grid |> Array2D.map f

        let builder = new StringBuilder()

        cells
        |> Array2D.iterf
            (fun cell ->
                cell |> builder.Append |> ignore)
            (fun () ->
                "\n" |> builder.Append |> ignore)

        builder.ToString()

    let visualize (Grid(grid)) =
        grid |> visualizef Cell.toChar


module World =
    let grid (world: World) = world.Grid

    let visitedCells (world: World) = world.VisitedCells

    let robot (world: World) = world.Robot


module Robot =
    let position (robot: Robot) = robot.Position


module AttributedGrid =
    let ofGrid (Grid(grid)) =
        grid
        |> Array2D.map (fun cell -> (cell, CellAttributeSet.empty) |> AttributedCell)
        |> AttributedGrid
    
    let update (Position(x, y)) f (AttributedGrid(grid) as g) =
        let (AttributedCell(cell, attr)) = grid.[x, y]
        grid.[x, y] <- f cell attr |> AttributedCell
        g


    let addAttribute pos (attr: CellAttribute) (grid: AttributedGrid) =
        grid |> update pos (fun cell (CellAttributeSet(attrs)) ->
            cell, (attrs |> Set.add attr |> CellAttributeSet))

    let ofWorld (world: World) =
        let grid = world |> World.grid |> ofGrid

        world |> World.visitedCells |> Seq.iter (fun pos ->
            grid |> addAttribute pos Visited |> ignore)

        let pos = world |> World.robot |> Robot.position
        in  grid |> addAttribute pos Robot |> ignore

        grid

    let visualize (AttributedGrid(grid)) =
        grid |> Grid.visualizef AttributedCell.toChar


module RuleKey =
    let create (state, surroundings) =
        { Initial = state
        ; Surroundings = surroundings |> Map.ofList
        }


module RuleValue =
    let create (direction, next) =
        { Direction = direction
        ; Next = next
        }


module Rule =
    let create k v =
        RuleKey.create k, RuleValue.create v


module RuleSet =
    let empty : RuleSet = Map.ofList [] |> RuleSet
